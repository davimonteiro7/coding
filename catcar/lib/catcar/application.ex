defmodule Catcar.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Catcar.Repo,
      # Start the Telemetry supervisor
      CatcarWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Catcar.PubSub},
      # Start the Endpoint (http/https)
      CatcarWeb.Endpoint
      # Start a worker by calling: Catcar.Worker.start_link(arg)
      # {Catcar.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Catcar.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    CatcarWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
