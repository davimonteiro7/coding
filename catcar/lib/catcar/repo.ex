defmodule Catcar.Repo do
  use Ecto.Repo,
    otp_app: :catcar,
    adapter: Ecto.Adapters.Postgres
end
