defmodule CatcarWeb.PageController do
  use CatcarWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
